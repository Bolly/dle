# DLE

Aplicación web no oficial del diccionario de la lengua española de la RAE para Ubuntu Touch.

## Web

https://dle.rae.es


[![OpenStore](https://open-store.io/badges/es.svg)](https://open-store.io/app/dle.bolly)
